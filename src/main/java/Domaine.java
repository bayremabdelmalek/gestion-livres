import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;
@Entity
public class Domaine implements Serializable {

    private String nomDomaine;

    public Domaine(String nomDomaine) {
        this.nomDomaine = nomDomaine;
    }

    public String getNomDomaine() {
        return nomDomaine;
    }

    public void setNomDomaine(String nomDomaine) {
        this.nomDomaine = nomDomaine;
    }
}


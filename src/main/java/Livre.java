import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;
@Entity
public class Livre implements Serializable {

    private String titre;

    public Livre(String titre) {
        this.titre = titre;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }
}

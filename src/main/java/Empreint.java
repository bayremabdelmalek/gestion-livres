import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import javax.persistence.*;
@Entity
public class Empreint implements Serializable {

    private Date dateRetour;

    public Empreint(Date dateRetour) {
        this.dateRetour = dateRetour;
    }

    public Date getDateRetour() {
        return dateRetour;
    }

    public void setDateRetour(Date dateRetour) {
        this.dateRetour = dateRetour;
    }
}
